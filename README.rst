Testeur pour EasyPython
=======================

Contient les modules qui permettent de tester des propriétés de solution d'exercices


Le format de sortie d'un testeur est une chaine obtenue en serialisant avec un jsonpickle un dictionnaire avec les clées : 
"_valide" -> True si l'exercice est validé, False sinon
"_messages" -> Dictionnaire associant à un titre une liste de messages d'erreur

Exemple : 
{ "_valide":False, "_messages":{"Erreur de copilation":["Probleme ligne 3 :...", "Problème ligne 123"]}}
ou encore:
{ "_valide":True, "_messages":{}}

