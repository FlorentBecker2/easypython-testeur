package easypython;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.ToolProvider;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.nio.file.Files;
import org.json.JSONObject;

class JavaSourceFromString extends SimpleJavaFileObject {
    final String code;

    JavaSourceFromString(String name, String source) {
        super(URI.create("string:///" + name ),Kind.SOURCE);
        this.code = source;
    }

    @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) {
            return code;
        }
}

public class CompileJava {
    public static Map<String,Object> compile(String source, String... options) {

        javax.tools.JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        HashMap<String,Object> res = new HashMap<String,Object>();

        if (compiler == null)
        {
            res.put("_valide",false);
            return res;
        }
        String code_source;
        try{
        byte[] encoded = Files.readAllBytes(Paths.get((String) options[0]));
        code_source = new String(encoded,StandardCharsets.UTF_8);
        }
        catch(IOException e)
        {
            res.put("_valide",false);
            return res;
        }
        StringWriter err = new StringWriter();
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
        ArrayList<String> compilerOptions = new ArrayList<String>();

        String[] opts = new String[options.length - 1];
        System.arraycopy(options, 1, opts, 0, opts.length);

        if (options != null) {
            compilerOptions.addAll(Arrays.asList(opts));
        }

        JavaFileObject codeSolution = new JavaSourceFromString(options[0], code_source );

        Iterable<? extends JavaFileObject> units = Arrays.asList(codeSolution);
        javax.tools.JavaCompiler.CompilationTask task = compiler.getTask(err, null, diagnostics, compilerOptions, null, units);

        boolean success = task.call();

        if (!success) {
            ArrayList<String> erreurs = new ArrayList<String>(); 
            for (Diagnostic diagnostic : diagnostics.getDiagnostics()) {
                if(diagnostic.getKind() == Diagnostic.Kind.ERROR){
                    String message = "ligne " + diagnostic.getLineNumber()+ "\n"; 
                    message += diagnostic.getMessage(null);
                    erreurs.add(message);
                }
            }
            res.put("_valide",false);
            HashMap<String,ArrayList<String>> messages = new HashMap<String,ArrayList<String>>();
            messages.put("Erreur de compilation",erreurs);
            res.put("_messages",messages);
            return res;
        }

        return res;
    }

    public static void main(String[] args){

        System.out.println(new JSONObject(compile("public class Machin{}", args))) ;

    }
}

