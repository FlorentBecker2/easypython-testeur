import sqlalchemy
import json

"""
   permet l'execution d'un script SQL.
   ATTENTION ne marche pas si une chaine de caractère MYSQL contient un ;
   
"""
def executeScript(connexion,script):
  listeLignes=script.split("\n")
  aux=''
  for i in range(len(listeLignes)):
    ind=listeLignes[i].find("--")
    if ind!=-1:
      aux+=listeLignes[i][:ind]+"\n"
    else:
      aux+=listeLignes[i]+'\n'
  listeCommandes=aux.split(";")
  for commande in listeCommandes:
    connexion.execute(commande)
"""
   transforme une ligne d'un curseur en une chaine de caractères lisible par l'étudiant
   ATTENTION risque de ne pas marcher si pour des types un peu exotiques
"""
def toStr(ligne):
  res='('
  pref=''
  for i in range(len(ligne)):
    if str(type(ligne[i])).startswith("<class 'decimal."):
      res+=pref+str(ligne[i])
    else:
      res+=pref+"'"+str(ligne[i])+"'"
    pref=','
  res+=')'
  return res

"""
   Compare le résultat de deux requêtes et returne une liste 
   de contre exemples si les résultats sont différents
"""
def comparer(resEtudiant,resCor,nbExemples=5):
  lEtudiant=list(resEtudiant)
  lCor=list(resCor)
  exemples=[]
  i=0
  nbPb=0
  while i< min(len(lEtudiant),len(lCor)) and nbPb<nbExemples:
    if lEtudiant[i]!=lCor[i]:
      nbPb+=1
      if lEtudiant[i] not in lCor:
        exemples.append("Résultat erronné: "+toStr(lEtudiant[i]))
      elif lCor[i] not in lEtudiant:
        exemples.append("Résultat manquant: "+toStr(lCor[i]))
      else:
        exemples.append("Problème éventuel de tri du résultat: "+toStr(lEtudiant[i]))
    i+=1
  while i< len(lEtudiant) and nbPb<nbExemples:
    if lEtudiant[i] not in lCor:
        exemples.append("Résultat erronné: "+toStr(lEtudiant[i]))
        nbPb+=1
    i+=1
  while i< len(lCor) and nbPb<nbExemples:
    if lCor[i] not in lEtudiant:
        exemples.append("Résultat manquant: "+toStr(lCor[i]))
        nbPb+=1
    i+=1
  return exemples

"""
  fonction de correction d'un exercice
  uri est une chaine de caractères donnant la manière de se connecter à la BD
      exemple: 'mysql+pymysql://limet:limet@servinfo-db/Sondage'
      ici on a le pb de login et mot de passe qui seront en clair quelque part
  script est un script SQL exécuté avant le test de correction
  equeteEtudiant est la requête fournie par l'étudiant
  requeteCor est le corrigé
  Resultat: un dictionnaire {"_valide":bool, "_message":dictionnaire de messages}
"""
def corriger(uri,script,requeteEtudiant,requeteCor):
  
  engine=sqlalchemy.create_engine(uri)
  cnx=engine.connect()
  if script!=None and script!='':
    executeScript(cnx,script)
  
  # on extrait uniquement la première instruction de la réponse en enlevant les espaces
  requeteEtud=requeteEtudiant.lstrip()

  feedback={"_valide":True,"_messages":{}}    
  # verification que la requête commence bien par select (pour eviter les injections de code)
  if requeteEtudiant[:6].upper()!='SELECT':
    feedback["_valide"]=False
    feedback["_messages"]={'Requete non valide':['Votre requete doit commence par le mot clé SELECT',"La requête n'a pas été exécutée"]}
    return feedback
  
  if requeteEtudiant.count(";")>1:
    feedback["_messages"]={"Requêtes multiple":["Attention votre réponse semble contenir plusieurs ordres SQL!\nSeul le premier sera exécuté!"]}
  
  # on essaie d'exécuter la requête de l'étudiant
  finReq=requeteEtudiant.find(';')
  if finReq<0:
    finReq=len(requeteEtudiant)
  try:
    resEtudiant=cnx.execute(requeteEtudiant[:finReq])
  except Exception as e:
    feedback["_messages"]={"Erreur de syntaxe":["Votre requête n'est pas syntaxiquement correcte\n"+str(e)]}
    feedback["_valide"]=False
    return feedback
  
  # on exécute la requête du corrigé
  # on considère que cette requête est syntaxiquement correcte
  resCor=cnx.execute(requeteCor)

  # test sur les colonnes
  colEtudiant=resEtudiant.keys()
  colCor=resCor.keys()

  if len(colEtudiant)!=len(colCor):
    feedback["_messages"]["Erreur de format"]=["Votre requête retourne "+str(len(colEtudiant))+ " colonnes alors qu'on en attend "+str(len(colCor)),
                                              "Voici la liste des colonnes attendues "+str(colCor)]
    feedback["_valide"]=False
    return feedback
  
  if colEtudiant!=colCor:
    feedback["_messages"]["Erreur de format"]=["Le nom des colonnes ou l'ordre des colonnes ne correspondent pas à ce qui est attendu",
                                              "Voici la liste des colonnes attendues "+str(colCor)]
    feedback["_valide"]=False
    return feedback
    
  
  # test sur le nombre de lignes
  if resEtudiant.rowcount!=resCor.rowcount:
    feedback["_valide"]=False      
    feedback["_messages"]["Résultat erronné"]=["Votre requête retourne "+str(resEtudiant.rowcount)+" lignes au lieu de "+str(resCor.rowcount)+" attendues"]
    if resEtudiant.rowcount<resCor.rowcount:
      feedback["_messages"]["Résultat erronné"].append("Il vous manque des résultats. Vérifiez les conditions de sélection (clauses WHERE)")
    else:
      feedback["_messages"]["Résultat erronné"].extend(["Vous avez trop de résultats! Cela peut être dû à","l'absence du mot clé DISTINCT après le SELECT",
                                                        "un problème de jointure entre des tables qui n'ont pas d'attributs commun",
                                                        "l'absence d'une condition dans une clause WHERE"])
  
  exemples=comparer(resEtudiant,resCor)
  
  if exemples==[]:
    if feedback["_valide"]:
      #feedback lorsque tout est ok
      feedback["_messages"]={"Félicitations!":["Réponse correcte"]}
    return feedback
  else:
    feedback["_valide"]=False
    feedback["_messages"]["Exemples de problèmes"]=exemples
    return feedback


if __name__ == "__main__":
    import argparse
    import yaml
    parser = argparse.ArgumentParser( formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("solution_etudiant",help="Fichier contenant la solution",type=argparse.FileType('r', encoding="utf-8"))
    parser.add_argument("solution_enseignant",help="Fichier contenant la solution",type=argparse.FileType('r', encoding="utf-8"))
    args = parser.parse_args()
    
    try:
        sol_ens=yaml.load(args.solution_enseignant)
    except yaml.YAMLError as exc:
        print(exc)
        exit(0)
    sol_etu="\n".join(args.solution_etudiant)
    
    print(json.dumps(corriger(sol_ens["uri"], sol_ens.get("script",""), sol_etu, sol_ens["solution"])))
    
    # exemple valide

# exemples avec des messages d'erreur




