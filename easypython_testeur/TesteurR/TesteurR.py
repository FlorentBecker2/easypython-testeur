__author__ = 'julien'
from ..Testeur import Testeur
from string import Template
import os
import shutil



class TesteurIJVM(Testeur):
    
    nom = "R"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def writeTestFiles(self, directory):

        with open(os.path.join(directory, "enonce_enseignant.r"), "w", encoding="utf-8") as solution_ens:
            solution_ens.write(self.codeTest.decode())

        with open(os.path.join(directory, "solution_etudiant.r"), "w", encoding="utf-8") as solution_etudiant:
            solution_etudiant.write(self.codeATester)
        
        with open(os.path.join(directory, "makefile"), "w", encoding="utf-8") as file:
            file.write("all:\n\t/usr/bin/Rscript --vanilla --slave testeurR.r enonce_enseignant.r solution_etudiant.r 2> /dev/null\n\ninfos:\n\techo {}")

        shutil.copyfile(os.path.join(os.path.dirname(__file__), "testeurR.r"),
                        os.path.join(directory, "testeurR.r"))
