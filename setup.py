from setuptools import setup, find_packages

setup(
    name='easypython-testeur',
    version='0.13',
    author='Julien Robert',
    author_email='julien.robert@univ-orleans.fr',
    packages=find_packages(),
    install_requires=['jsonpickle', 'psutil'],
    dependency_links=[],
    package_data={'easypython_testeur.TesteurJava': [
        'hamcrest-core-1.3.jar', 'junit-4.12.jar'], }
)
